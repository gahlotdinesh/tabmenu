import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirsttabsPage } from './firsttabs.page';

describe('FirsttabsPage', () => {
  let component: FirsttabsPage;
  let fixture: ComponentFixture<FirsttabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirsttabsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirsttabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
